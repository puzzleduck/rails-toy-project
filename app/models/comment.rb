class Comment < ApplicationRecord
  belongs_to :mini_blog
  validates :text, length: { minimum: 1 },
                   length: { maximum: 140 }
end
