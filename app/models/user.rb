class User < ApplicationRecord
  has_many :mini_blogs
  validates :name, presence: true
  validates :email, presence: true

end
