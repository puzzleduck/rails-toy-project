class MiniBlog < ApplicationRecord
  validates :content, length: { maximum: 14 },
                      presence: true
  belongs_to :user
  has_many :comments
end
