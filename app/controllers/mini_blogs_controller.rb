class MiniBlogsController < ApplicationController
  before_action :set_mini_blog, only: [:show, :edit, :update, :destroy]

  # GET /mini_blogs
  # GET /mini_blogs.json
  def index
    @mini_blogs = MiniBlog.all
  end

  # GET /mini_blogs/1
  # GET /mini_blogs/1.json
  def show
  end

  # GET /mini_blogs/new
  def new
    @mini_blog = MiniBlog.new
  end

  # GET /mini_blogs/1/edit
  def edit
  end

  # POST /mini_blogs
  # POST /mini_blogs.json
  def create
    @mini_blog = MiniBlog.new(mini_blog_params)

    respond_to do |format|
      if @mini_blog.save
        format.html { redirect_to @mini_blog, notice: 'Mini blog was successfully created.' }
        format.json { render :show, status: :created, location: @mini_blog }
      else
        format.html { render :new }
        format.json { render json: @mini_blog.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /mini_blogs/1
  # PATCH/PUT /mini_blogs/1.json
  def update
    respond_to do |format|
      if @mini_blog.update(mini_blog_params)
        format.html { redirect_to @mini_blog, notice: 'Mini blog was successfully updated.' }
        format.json { render :show, status: :ok, location: @mini_blog }
      else
        format.html { render :edit }
        format.json { render json: @mini_blog.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /mini_blogs/1
  # DELETE /mini_blogs/1.json
  def destroy
    @mini_blog.destroy
    respond_to do |format|
      format.html { redirect_to mini_blogs_url, notice: 'Mini blog was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_mini_blog
      @mini_blog = MiniBlog.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def mini_blog_params
      params.require(:mini_blog).permit(:title, :content, :user_id)
    end
end
