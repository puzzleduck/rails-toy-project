json.extract! mini_blog, :id, :title, :content, :user_id, :created_at, :updated_at
json.url mini_blog_url(mini_blog, format: :json)