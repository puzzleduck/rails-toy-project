require 'test_helper'

class MiniBlogsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @mini_blog = mini_blogs(:one)
  end

  test "should get index" do
    get mini_blogs_url
    assert_response :success
  end

  test "should get new" do
    get new_mini_blog_url
    assert_response :success
  end

  test "should create mini_blog" do
    assert_difference('MiniBlog.count') do
      post mini_blogs_url, params: { mini_blog: { content: @mini_blog.content, title: @mini_blog.title, user_id: @mini_blog.user_id } }
    end

    assert_redirected_to mini_blog_url(MiniBlog.last)
  end

  test "should show mini_blog" do
    get mini_blog_url(@mini_blog)
    assert_response :success
  end

  test "should get edit" do
    get edit_mini_blog_url(@mini_blog)
    assert_response :success
  end

  test "should update mini_blog" do
    patch mini_blog_url(@mini_blog), params: { mini_blog: { content: @mini_blog.content, title: @mini_blog.title, user_id: @mini_blog.user_id } }
    assert_redirected_to mini_blog_url(@mini_blog)
  end

  test "should destroy mini_blog" do
    assert_difference('MiniBlog.count', -1) do
      delete mini_blog_url(@mini_blog)
    end

    assert_redirected_to mini_blogs_url
  end
end
