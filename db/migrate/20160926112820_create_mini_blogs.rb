class CreateMiniBlogs < ActiveRecord::Migration[5.0]
  def change
    create_table :mini_blogs do |t|
      t.string :title
      t.string :content
      t.integer :user_id

      t.timestamps
    end
  end
end
