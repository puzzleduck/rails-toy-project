Rails.application.routes.draw do
  resources :mini_blogs do
    resources :comments
  end
  resources :users
  root "application#hi"
  get "secret/" => "application#secret"
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
